I wrote in my NAC application last year about how I believe the civic tech community inflicts the world with many of the same flaws and arrogance of the so called "tech" community (Silicon Valley).
I still think we have some growing up to do, learning from community organizers, activists, academic researchers and other change makers.

But, for the Network specifically, I would like to see us do a better job of pooling our resources and sustaining projects with community input.
It seems we could do so much more if we coordinated our efforts, reduced rework, and even specialized a bit.
We could be pooling resources to all help one brigade finish their next project and then pay it forward in a rotation.
Or, perhaps more realistically, we could try to methodically spread our most successful projects (e.g. Court Bot, Clear My Record, GetCalFresh) one brigade at a time.
If I were elected, I would love to help facilitate more partnerships between brigades to help support each others' projects.
