I co-led the unconference portion of the 2019 Brigade Conference.
I split my time running Code for Nashville and doing data consulting for healthcare and government.
I am passionate about DEI, data, transparency, and direct democracy.
Learn more at tim4nac.us. :)
