I am constantly looking for new qualitative and quantitative ways to identify (and hopefully counter) everything from systemic issues down to my own microaggressions and privilege.

When I joined leadership of Code for Nashville 2 years ago, I advocated for a new position of Community and Diversity Lead.
I took the position because sadly, all four of our leads (includig me) were cis white men, and our brigade membership wasn't much more diverse than that (particularly by race).
We did a mixer with Black In Tech using their rubric to audit City services for inequities.
I learned new peoples' names before events so I could do a better job welcoming them.
I recruited someone to critically study the impact of our group on people of color (for her PhD).
I conducted seven 1-hour interviews after a near violation of our Code of Conduct.
Based on that, we started an oboarding process to help protect project groups from white men with fragile egos who felt their experience trumped everything else.
I got more involved with the Movement for Black Lives (including our city's new Community Oversight Board for the Police) and talked about issues and possible partnerships more at our Meetups.
I started revitalizing our oldest project (incluCivics.org) which is a dashboard of inequities in salaries and placements by race and gender in our city government.
I have actively recruited women and people of color to lead projects and present to the general membership.
I have worked with our speakers to help them understand and appreciate one our differently-abled members who asks a lot of pointed questions.
We meet in a public library so we are available to the public and I've tried to make sure that people dropping for the food feel personally welcomed and take as extra for the road.
We are looking for public spaces in marginalized neighborhoods, but our city libraries have shorter hours and inequitable services in those neighborhoods. :(
We've found that Latinx people are grossly underrepresented in our brigade and our city government, so I am recruiting for a new position of Latinx Community Liaison to help cross-recruit, share ideas and feedback, and build partnerships.
If we get grants or contracts, I would like to prioritize recruiting People of Color as critical and technical partners to receive stipends.

Our biggest problem is retaining people who are experience marginalization and are willing to tell us what we could do differently.
I think that the NAC and the Network Team could give us feedback and tips to recruit enough leaders, volunteers, and outside feedback that we can learn and improve without tokenizing anybody.
