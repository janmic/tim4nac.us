# Run for the 2020 National Advisory Council
1. Bio - In fewer than 280 characters, introduce yourself to members of the Brigade Network who may not know you.
 
2. The civic tech community is growing up, and I'll help it continue to mature by... *
Please answer in fewer than 1000 characters.
 
3. What do you see as a key problem that the NAC should help the Network solve in the next year? *
Please answer in fewer than 1000 characters.
 
4. How will you balance your work with your local Brigade with serving on the National Advisory Council? *
Please answer in fewer than 1000 characters.
 
5. How can the NAC and the Network team support your efforts to make your brigade more diverse and inclusive? Can you speak to what work you've already done in this area? *
Please answer in fewer than 1000 characters.
 
