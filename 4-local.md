I worry that changes in CfA leadership may limit the support the Network Team gives Brigades.

I wasn't planning to run for NAC this year because I wanted to get Code for Nashville ready in case we needed to operate without CfA.
We just started defining our formal membership so we can start having elections for leadership.
And, I was hoping to start securing grants and contracts to help support stipends and scholarships for additional co-leads and volunteers.

However, several people asked me this year if I would consider running to share these concerns publicly that (I believe) many of us share privately.
I could work on strengthening my Brigades, but since this impacts all of us, if I am elected, I will cut back as much as possible on Code for Nashville to advocate for Brigades in the NAC.
Our leadership team feels confident they could maintain the Brigade by cutting back on some projects and slowing our plans for the future.

No matter what happens, I will continue to develop direct Brigade to Brigade relationships, because I believe we need each other to thrive.
