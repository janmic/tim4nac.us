I think the Brigade Network's greatest strengths and potential are in its grassroots informal powers like volunteerism, self-organization, and transparency.

I imagine this gets awkward for a hierarchical (non-profit) consulting company like Code for America to manage.
For example, CfA doesn't publish the NAC election results.
I think that strains the democratic process.
I don't think they undermine the Network on purpose - I just think that are organized so differently that it's hard for them to anticipate our needs.

I would like to see the NAC's use its informal powers to gain the formal powers it needs properly advocate for the Network.
For example, the NAC could prepare a budget for the Network team and share it publicly to put pressure on CfA to include the Brigades in their own support.

Elected or not, I will continue to assume the best of intentions and try work *with* CfA, but only to the extent that I feel works *for* the Brigades.
